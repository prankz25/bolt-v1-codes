
#include <SoftwareSerial.h>

SoftwareSerial mySerial(10, 11); // RX, TX
void setup() {

  Serial.begin(9600);     //Serial monitor
  mySerial.begin(9600);   //Bolt Rx-Tx
  pinMode(13, OUTPUT);

}

char x;
String cmd;

void loop() {

  if (mySerial.available() > 0)
  {
    cmd = "";
    while (mySerial.available() > 0)
    {
      x = mySerial.read();
      Serial.println(x);
      cmd += String(x);
    }
    cmd.trim();
    cmd.toLowerCase();
    Serial.println("This is the command");
    Serial.println(cmd);
    Serial.println(cmd.length());
    if (cmd.equals("on"))
    {
      Serial.println("Glowing the led");
      digitalWrite(13, HIGH);
    }
    else if (cmd.equals("off"))
    {
      Serial.println("putting the led of");
      digitalWrite(13, LOW);
    }
    else {
      Serial.println("Command not defined");
    }
  }

}
